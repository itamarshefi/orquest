#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Item(object):
    """The base class for all items"""

    def __init__(self, name, description, status='ok'):
        self.name = name
        self.description = description
        self.status = status

    def __str__(self):
        return self.name

    def examine(self):
        return u'{}. {}'.format(self.name, self.description)

    def as_dict(self):
        return {'name': self.name,
                'description': self.description,
                'status': self.status}


class Tati(Item):
    def __init__(self, name=u'טופס-טיולים', description=u'חייבים להשיג את כל החתימות בשביל לגזור חוגר ולהשתחרר!', status=None):
        super(Tati, self).__init__(name, description, status)

    def examine(self):
        s = super(Tati, self).examine()
        if self.status:
            s += u'\nחתימות חסרות:\n' + u'\n'.join(self.status)
        return s

    def give_tati(self):
        self.status = [u'רסר', u'אפסנאי', u'טכנאי', u'מזמ']

    def sign_tati(self, signer):
        if signer in self.status:
            self.status.remove(signer)

    def did_sign(self, room):
        return (self.status is not None) and room.name in self.status
