#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import world
from player import Player
import json
import time
import codecs
import curses
from random import random, choice
import os
import locale
locale.setlocale(locale.LC_ALL, '')
code = locale.getpreferredencoding()

try:
    import _thread as thread
    import curses
    screen = curses.initscr()
    screen.clear()
    screen.border(0)
    curses.curs_set(0)
    curses.noecho()
    CURSES = True
except Exception as e:
    print("Consider installing curses. It improves gameplay!")
    screen = None
    CURSES = False

TITLE_THRESH = 0.3
try:
    with open('resources/titles', 'r') as f:
        titles = f.read().split('+')
except:
    titles = [u'אור לוי קוסט']

allowed_actions = {u'לכי ל': u'בחרי את המקום אליו את רוצה ללכת',
                   u'חזרה': u'כמו לכי, אבל יחזיר אותך למקום הקודם שהיית בו',
                   u'השתמשי ב': u'השתמשי בחפץ',
                   u'קחי ...': u'קחי חפץ )מהחפצים שנמצאים בחדר(',
                   u'תני ... ל': u'תני אחד מהחפצים שלך למישהו',
                   u'דברי עם': u'דברי עם מישהו בחדר',
                   u'חפצים': u'מדפיס רשימה של החפצים שברשותך',
                   u'הסתכלי': u'בחני את החדר. אפשר גם על חפץ שנמצא בסביבה/עלייך',
                   u'עזרה': u'מדפיס את ההודעה הזאת',
                   u'יציאה': u'צאי מהמשחק'}


intro = u'''
בוקר. הרדיו דולק.
"בוקר טוב, השעה 00:01. כאן נוגה קליין עם מה שקורה עכשיו"
מה שקורה עכשיו...
מה בעצם קורה עכשיו?
אה, כן! את משתחררת היום!
ונוגה עדיין מגישה חדשות?!

מה עוד מחכה לך?
'''

def play():
    world.load_tiles()
    player = Player()
    s = title_screen(player)
    if not s:
        lprint(intro,  3)
    done = True
    while not player.victory:
        if done:
            lprint(u'לחצי על מקש כלשהו כדי להמשיך...', -1)
            screen.get_wch()
        Y = 3
        if CURSES:
            screen.clear()
            screen.border(0)
        room = world.world[player.location]
        interaction = room.interact_with_player(player)
        lprint(interaction, Y)
        Y += interaction.count('\n') + 2
        screen.refresh()
        life_changing_action = False
        while not life_changing_action:
            if not player.victory:
                if not CURSES:
                    raw_action_args = input(("\n")).decode('utf-8')
                else:
                    maxy, maxx = screen.getmaxyx()
                    YLINE = maxy - 3
                    XROW = maxx - 5
                    screen.addstr(YLINE, XROW + 1, ' <')
                    screen.refresh()
                    raw_action_args = ''
                    c = ''
                    while c != '\n':
                        raw_action_args += c
                        screen.addstr(YLINE, XROW - len(raw_action_args), c)
                        c = screen.get_wch()
                        if ord(c) in (curses.KEY_BACKSPACE, 8, 127):
                            if raw_action_args:
                                raw_action_args = raw_action_args[:-1]
                                screen.addstr(YLINE, XROW - 1 - len(raw_action_args), ' ')
                                screen.refresh()
                            c = ''
                        else:
                            c = str(c)
                    # raw_action_args = screen.getstr(maxy - 4, maxx - 4, 60).decode('utf-8').strip()
                    screen.addstr(YLINE, XROW + 1, ' <')
                    screen.refresh()
                write_log('> ' + raw_action_args)
                action_args = raw_action_args.split()
                if action_args:
                    done, life_changing_action = do_action(player, action_args)
                    if done is None and life_changing_action is None:
                        return
                    if done:
                        lprint(done, Y)
#    lprint(u'זהו! השתחררת\nמזל טוב והמון אהבה.\nאני יודע שתצליחי 3<\n\t\tאוהב, נמושון', y=20)

def do_action(player, action_args):
    action = action_args.pop(0)
    if action == u'עזרה':
        help_string = u'אלו הפקודות שאת יכולה להשתמש בהן במשחק:\n'
        for action, description in allowed_actions.items():
            help_string += u'{}: {}\n'.format(action, description)
        return help_string, False
    elif action == u'חפצים':
        return player.inventory_string(), False
    elif action == u'לכי':
        return player.move(remake_args(action_args))
    elif action == u'חזרה':
        return player.move_back()
    elif action == u'השתמשי':
        if not validate_transitive(u'ב', action_args):
            return u'במה להשתמש?', False
        else:
            return player.use(remake_args(action_args))
    elif action == u'תני':
        if not validate_dtransitive(u'', u'ל', action_args):
            return u'מה לתת? למי?', False
        else:
            return player.give(action_args[0], remake_args(action_args[1]))
    elif action == u'קחי':
        return player.take(remake_args(action_args, 0)), False
    elif action == u'הסתכלי':
        if len(action_args) > 1:
            return player.examine_item(action_args[1]), False
        else:
            return player.look_around(), False
    elif action == u'דברי':
        if len(action_args) < 2 or action_args[0] != u'עם':
            return player.talk(u'')
        else:
            return player.talk(action_args[1])

    elif action == u'שמירה':
        try:
            save(player)
            return u'המשחק נשמר', False
        except Exception as e:

            return u'בעיה בשמירת המשחק' + str(e), False
    elif action == u'טעינה':
        return load_game(player)
    elif action == u'יציאה':
        return None, None
    else:
        return u'פקודה לא מוכרת. הקישי "עזרה" בשביל רשימת הפקודות החוקיות', False

def load_game(player):
    try:
        load(player)
        return u'המשחק נטען', True
    except Exception as e:
        write_log(str(e))
        return u'בעיה בטעינת המשחק', False

def validate_transitive(preposition, action_args, special=None):
    return len(action_args) > 0 and (preposition in action_args[0][0] or special == action_args[0])


def validate_dtransitive(p1, p2, action_args):
    return len(action_args) > 1 and validate_transitive(p1, action_args[:1]) and validate_transitive(p2, action_args[1:])


def remake_args(action_args, pre_len=1):
    return ''.join(action_args)[pre_len:]


def save(player):
    player_d = player.as_dict()
    world_d = world.as_dict()
    with open('saved_game', 'w') as f:
        json.dump({'player': player_d, 'world': world_d}, f)


def load(player):
    with open('saved_game', 'r') as f:
        save_data = json.load(f)
    player.load_player(**save_data['player'])
    world.load_world(save_data['world'])


def add_he_str(screen, y, string, machine_type=True):
    _, x = screen.getmaxyx()
    wait = 0.05 if machine_type else 0
    try:
        for i, c in enumerate(string):
            screen.addstr(y, x - i - 4, c.encode(code).decode(code))
            time.sleep(wait)
            screen.refresh()
    except:
        raise Exception(string)


def lprint(s, y=3):
    write_log(s)
    drct = -1
    # string_lines = [l[::drct] for l in s.split('\n')]
    string_lines = s.split('\n')
    if CURSES:
        maxy, maxx = screen.getmaxyx()
        if y == -1:
            y = maxy - 1
        for j in range(y, maxy-1):
            screen.addstr(j, 0, ' ' * maxx)
        screen.border(0)
        screen.refresh()
    for i, l in enumerate(string_lines):
        if CURSES:
            add_he_str(screen, y + i, l)
        else:
        # try:
        #  _, l = os.popen('stty size', 'r').read().split()
        #   l = '0'
        #     drct = -1
        #  except:
        # pass
            print (s[::drct]) #.rjust(int(l))
        time.sleep(0.1)

def title_screen(player):
    if not CURSES:
        if random() > TITLE_THRESH:
            title_lines = choice(titles)
        else:
            title_lines = titles[-1]
        lprint(title_lines)
        time.sleep(3)
        lprint(u'1. משחק חדש\n2. טעינה'[::-1])
        st = ''
        while st not in ['1', '2']:
            st = str(input())
    else:
        Cond = []
        thread.start_new_thread(update_title, (screen, Cond))
        st = ''
        while st not in ['1', '2']:
            st = str(screen.get_wch())
        Cond.append(1)
        screen.clear()
        screen.border(0)
        screen.refresh()
    if st == '2':
        _, loaded = load_game(player)
        if not loaded:
            lprint(u'שגיאה בטעינת המשחק. מתחיל משחק חדש')
        else:
            return True
    return False

def update_title(screen, cond):
    while not cond:
        for title in titles:
            y, x = screen.getmaxyx()
            w = title.find('\n', 1)
            h = title.count('\n')
            if w >= x or h >= y:
                continue
            screen.clear()
            screen.border(0)
            for i, line in enumerate(title.split('\n')):
                screen.addstr(int((y - h) / 2 + i), int((x - w) / 2), line)
            # lprint(u'1. משחק חדש\n2. טעינה', y - 2, machine_type=False)s
            add_he_str(screen, y - 4, u' 1. משחק חדש', machine_type=False)
            add_he_str(screen, y - 3, u'2. טעינה', machine_type=False)
            time.sleep(3)
            screen.refresh()
            if cond: break


def write_log(s):
    with codecs.open(logfile, 'a', 'utf-8') as lf:
        lf.write(s + '\n')

if __name__ == "__main__":
    if not os.path.exists('logs'):
        os.mkdir('logs')
    logfile = 'logs/{}.log'.format(time.asctime(time.localtime(time.time())))
    try:
        play()
        if CURSES:
            lprint(u'לחצי על מקש כלשהו כדי לצאת...', -1)
            screen.get_wch()
    except Exception as e:
        import traceback
        with open(logfile, 'a') as lf:
            traceback.print_exc(file=lf)
        print ( u'שגיאה במשחק. ראה לוג')
        print (logfile)
        traceback.print_exc()
    finally:
        time.sleep(1)
        if CURSES:
            curses.endwin()
