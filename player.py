# -*- coding: utf-8 -*-

import world
from items import Item, Tati
from tiles import LootRoom, PersonRoom


class Player():
    def __init__(self):
        self.inventory = [Tati()]
        self.location = world.starting_position
        self.prev_locs = []
        self.victory = False
        self.imd = {1: u'',
                    2: u'',
                    3: u'מתחיל ללחוץ קצת',
                    4: u'איזה כיף שבכיסים אזרחיים יש קצת יותר מקום מכיסים של מדים',
                    5: u'טוב, זה מגוחך כבר. לא נראה לי שבאמת יש שם מקום, אבל נגיד',
                    6: u'זה כיס או טארדיס?',
                    7: u'שמעי, זה לא משכנע כבר. הכיס יותר גדול ממך'}

    def load_player(self, tati_status, location, inventory, prev_locs, victory):
        self.inventory = [Tati()] + [Item(**item) for item in inventory]
        self.inventory[0].status = tati_status
        self.location = location
        self.prev_locs = prev_locs
        self.victory = victory

    def as_dict(self):
        return {'tati_status': self.inventory[0].status,
                'inventory': [item.as_dict() for item in self.inventory[1:]],
                'location': self.location,  # TODO simple class name
                'prev_locs': self.prev_locs,
                'victory': self.victory
                }

    def inventory_string(self):
        inventory_names = self._get_inventory_names()
        if not inventory_names:
            return u'אין לך כלום!'
        else:
            return u'יש לך:\n' + u'\n'.join(inventory_names)

    def move(self, dest):
        dest = u'הביתה' if dest == u'ביתה' else dest
        if not dest:
            return self._get_curr_room().loc_string(u'לאן ללכת?', self.get_prev_room_name()), False
        new_loc = world.is_neighbor(self.location, dest)
        if new_loc is None:
            return self._get_curr_room().loc_string(u'את לא יכולה ללכת ל{} מכאן! זה רחוק מדי'.format(dest), self.get_prev_room_name()), False
        else:
            new_loc = new_loc.raw_name
            if self._get_curr_room().is_locked() and new_loc != self.prev_locs[-1]:
                return u'אי אפשר ללכת ל{} עכשיו. אפשר רק לחזור אחורה.'.format(dest), False
            self.prev_locs.append(self.location)
            self.location = new_loc
            return u'', True

    def move_back(self):
        if not self.prev_locs:
            return u'זאת ההתחלה!', False
        else:
            self.location = self.prev_locs.pop(-1)
            return u'', True

    def use(self, item_name):
        item = self.get_item(item_name)
        if item:
            return self._get_curr_room().use(item, self)
        else:
            return u'אין לך {}! אולי כדאי להשיג אחד'.format(item_name), False

    def give(self, item_name, person):
        room = self._get_curr_room()
        item = self.get_item(item_name)
        if item:
            if isinstance(room, PersonRoom) and room.is_present and room.person == person:
                return self._get_curr_room().give(item, self)
            else:
                return u'אין כאן שום {}'.format(person), False
        else:
            return u'אין לך {}'.format(item), False

    def take(self, item):
        room = self._get_curr_room()
        if room.is_locked():
            return u'אי אפשר לקחת מכאן דברים עכשיו!'
        elif isinstance(room, LootRoom):
            item_names = [it.name for it in room.items]
            if item in item_names:
                item_idx = item_names.index(item)
                self.inventory.append(room.items.pop(item_idx))
                s = u'לקחת את ה{} ושמת בכיס. '.format(item)
                return s + self._inventory_message()
            pre = u'מה לקחת?\n' if not item else u'אין כאן {}!\n'.format(item)
            return pre + u'{}'.format(room.look_loot())
        elif not item:
            return u'מה לקחת?'
        else:
            return u'אין כאן {}! את יכולה לחפש במקומות אחרים'.format(item)

    def talk(self, person):
        room = self._get_curr_room()
        if isinstance(room, PersonRoom) and room.is_present:
            if person == u'':
                return u'עם מי לדבר?', False
            if person != room.person:
                return u'{} לא בחדר'.format(person), False
            else:
                talk, changer = room.talk()
                if 'TP' in talk:
                    talk, tp = talk.split('TP_')
                    world.trigger_present(tp)
                return talk, changer
        else:
            return u'אין פה אף אחד לדבר איתו!', False

    def examine_item(self, item_name):
        return self._get_curr_room().examine_item(item_name, self.inventory)

    def look_around(self):
        return self._get_curr_room().look_around(self)

    def _get_room(self, room):
        return world.world[room]

    def _get_curr_room(self):
        return self._get_room(self.location)

    def _inventory_message(self):
        return self.imd.get(len([item for item in self.inventory if item.status is not None]), u'ממש לא ברור איך את מכניסה הכל לשם. תכניסי גם פיל וזהו')

    def _get_inventory_names(self, cond=lambda item: item.status is not None):
        return [item.name for item in self.inventory if cond(item)]

    def get_item(self, item_name):
        item_names = self._get_inventory_names(cond=lambda item: item)
        if item_name in item_names:
            item_idx = item_names.index(item_name)
            item = self.inventory[item_idx]
            if item.status is not None:
                return item
        return None

    def get_prev_room_name(self):
        if self.prev_locs:
            return self._get_room(self.prev_locs[-1]).name

    def takbles(self):
        room = self._get_curr_room()
        if isinstance(room, LootRoom):
            return self._get_curr_room().look_loot()
        else:
            return u''