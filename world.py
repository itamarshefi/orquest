# -*- coding: utf-8 -*-
import json

world = {}
starting_position = 'Home'
from tiles import LootRoom, PersonRoom

def load_tiles():
    """Parses a file that describes the world space into the _world object"""
    global starting_position
    with open('resources/map.txt', 'r') as f:
        raw_world = json.load(f)
    for room in raw_world:
        world[room] = getattr(__import__('tiles'), room)(room)
        world[room].set_name()
    for room in world:
        neighbors = [world[n] for n in raw_world[room]]
        world[room].update_neighbors(neighbors)


def is_neighbor(room, neighbor):
    ns = world[room].neighbors
    room_names = [n.name for n in ns]
    if neighbor not in room_names:
        return None
    else:
        return ns[room_names.index(neighbor)]


def trigger_present(room):
    for c in world.values():
        if c is not None and c.name == room:
           c.is_present = True


def as_dict():
    world_dict = dict()
    for c, room in world.items():
        if room is not None:
            k = ','.join((str(x) for x in c))
            world_dict[k] = {'attributes': room.attributes}
            if isinstance(room, LootRoom):
                world_dict[k]['items'] = [item.as_dict() for item in room.items]
            if isinstance(room, PersonRoom):
                world_dict[k]['is_present'] = room.is_present
    return world_dict


def load_world(world_dict):
    for c, room_dict in world_dict.items():
        k = c.replace(',', '')
        world[k].load(room_dict)
