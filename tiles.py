# -*- coding: utf-8 -*-

from items import Item
import random


class MapTile(object):
    def __init__(self, raw_name, locked=False):
        self.raw_name = raw_name
        self.neighbors = []
        self.name = None
        self.attributes = {}
        self.attributes['visited'] = False
        self.attributes['locked'] = locked

    def is_locked(self):
        return self.attributes['locked']

    def update_neighbors(self, neighbors):
        self.neighbors = neighbors

    def interact_with_player(self, player):
        raise NotImplementedError()

    def set_name(self):
        raise NotImplementedError()

    def room_str(self, string):
        return string

    def loc_string(self, string, prev_loc):
        if string:
            string += '\n'
        if not self.attributes['locked']:
            return string + u'מכאן אפשר ללכת ל: ' + u', '.join([n.name for n in self.neighbors])
        else:
            return string + u'כרגע, מכאן אפשר ללכת ל' + prev_loc

    def look_around(self, player):
        return self.loc_string(u'', player.get_prev_room_name())

    def use(self, item, player):
        return u'מה... מה בדיוק חשבת לעשות עם {} פה?'.format(item)

    def examine_item(self, item_name, items=[]):
        item_names = [item.name for item in items]
        if item_name in item_names:
            item_idx = item_names.index(item_name)
            if items[item_idx].status is not None:
                return items[item_idx].examine()
        return u'אין שום {} בטווח הראיה שלך. אולי כדאי להחליף משקפיים?'.format(item_name)

    def load(self, room_d):
        self.attributes = room_d['attributes']

class LockedRoom(MapTile):
    def __init__(self, raw_name):
        super(LockedRoom, self).__init__(raw_name, True)


class LootRoom(MapTile):
    def __init__(self, raw_name, items, locked=False):
        self.items = items
        super(LootRoom, self).__init__(raw_name, locked=locked)

    def look_loot(self):
        s = u''
        if self.items and not self.attributes['locked']:
            s = u'יש כאן: ' + ', '.join([item.name for item in self.items])
        return s

    def look_around(self, player):
        return self.loc_string(self.look_loot(), player.get_prev_room_name())

    def examine_item(self, item_name, more_items=[]):
        all_items = self.items + more_items
        return super(LootRoom, self).examine_item(item_name, all_items)

    def load(self, room_d):
        self.items = [Item(**item_d) for item_d in room_d['items']]
        super(LootRoom, self).load(room_d)


class PersonRoom(MapTile):
    def __init__(self, raw_name, person, is_present, locked=False):
        self.person = person
        self.is_present = is_present
        super(PersonRoom, self).__init__(raw_name, locked=locked)

    def talk(self):
        raise NotImplementedError()

    def give(self, item, player):
        return u'המבט בעיניים של {} כשאת מגישה את ה{} אומר הכל.\n"מה לעזאזל את רוצה שאני אעשה עם זה? אני לא הפח זבל שלך"'.format(self.person, item), False

    def load(self, room_d):
        self.is_present = room_d['is_present']
        super(PersonRoom, self).load(room_d)

    def use(self, item, player):
        return self.give(item, player)


class Rasar(PersonRoom):
    def __init__(self, raw_name):
        super(Rasar, self).__init__(raw_name, u'הרסר', True)

    def interact_with_player(self, player):
        if player.inventory[0].status is None or u'רסר' not in player.inventory[0].status:
            return u'החדר של הרס"ר. מה יכול להיות יותר מפחיד מזה? הרס"ר יושב כאן מנומנם.'
        else:
            player.inventory[0].sign_tati(u'רסר')
            return u'"אור לוי??? בואי בואי בואי רגע... בטוח אני אחתום לך על הטופס טיולים,\nרק בואי תעזרי לי בשאלה על סדרות... צוחקים איתך יא אשכנזיה"\nחתם. ביי.'

    def talk(self):
        return u'הרסר ממלמל דברים לא ברורים', False


    def set_name(self):
        self.name = u'רסר'


class Ulpan(LootRoom):
    def __init__(self, raw_name):
        super(Ulpan, self).__init__(raw_name, [Item(u'מספריים', u'מצויינים לגזירת חוגרים')], locked=True)
        self.attributes['recorded'] = False

    def interact_with_player(self, player):
        if player.inventory[0].status is None or u'טכנאי' not in player.inventory[0].status or self.attributes['recorded']:
            return u'טכנאי יושב כאן ורואה טלויזיה. איזה כיף שיש שידורי תאגיד!'
        else:
            return u'יש פה טכנאי זב חוטם. "מה? אור לוי משתחררת? עדיין לא הקלטתי איתך כלום.\nתערכי איתי ואני אחתום לך! אבל את צריכה אוזניות"'

    def use(self, item, player):
        if item.name == u'אוזניות':
            self.attributes['locked'] = False
            self.attributes['recorded'] = True
            return u'אתם מקליטים, עורכים ומשתעשעים ב5 שעות עבודה שיכולת לסיים ב7 דקות באודסיטי\n"תודה רבה לך! הממלכה שלי היא שלך עכשיו, את יכולה לקחת מהאולפן הזה מה שאת רוצה!"',  False
        elif item.name == u'טופס-טיולים':
            if self.attributes['recorded']:
                player.inventory[0].sign_tati(u'טכנאי')
                return u'הטכנאי זב-החוטם חותם לך על הטופס!', False
            else:
                return u'הטכנאי בוכה וצורח. "קודם תערכי איתי!!!"', False
        else:
            return u'למה שתשתמשי בזה כאן?', False

    def set_name(self):
        self.name = u'אולפן'


class Home(LootRoom):
    def __init__(self, raw_name):
        super(Home, self).__init__(raw_name, [Item(u'רב-קו', u'מצויין לנסיעה באוטובוסים'), Item(u'מפתח', u'קצת חלוד, אבל אפשר לפתוח איתו דלתות וארוניות')])

    def interact_with_player(self, player):
        st = u'אין כמו הבית. הוא קצת מבולגן, כרגיל.'
        return self.room_str(st)

    def set_name(self):
        self.name = u'הביתה'


class BusStation(LootRoom):
    def __init__(self, raw_name):
        super(BusStation, self).__init__(raw_name, [Item(u'כסף', u'08 שקלים חדשים')])

    def interact_with_player(self, player):
        return self.room_str(u'את בתחנת אוטובוס. בדיוק נכנס אוטובוס לתחנה.')

    def set_name(self):
        self.name = u'גן-העיר'


class Bus(LockedRoom):
    def interact_with_player(self, player):
        if self.attributes['locked']:
            return u'ובכן, גברת. המדים שלך כבר בתוך הקיטבג. אין יותר נסיעות חינם.\nהנהג מסתכל עלייך ומבקש שתשלמי על הנסיעה - אבל הארנק שלך לא עלייך!'
        else:
            return self.loc_string(u'קו {} הארור. יאללה, עוד רגע מגיעים. בעצם - לאן את נוסעת?'.format(random.choice([81, 52, 521, '01'])), player.get_prev_room_name())

    def set_name(self):
        self.name = u'אוטובוס'

    def use(self, item, player):
        if item.name == u'כסף':
            return u'הנהג מתעצבן עלייך. "אין לי עודף מ08 שקלים. רדי מהאוטובוס"', False
        elif item.name != u'רב-קו':
            return u'הנהג מתעצבן עלייך. "את מוכנה לעוף מפה? מה אני אעשה עם {}"?'.format(item), False
        else:
            self.attributes['locked'] = False
            return u'הנהג מבסוט על התמונה המקסימה ברב-קו שלך. "אני לעולם לא אשכח אותך!"', True


class GlzEntrance(MapTile):
    def interact_with_player(self, player):
        return self.room_str(u'הרחבה בכניסה של גלצ עם התל אופן והספסלים וכל שאר הדברים הכיפיים.')

    def set_name(self):
        self.name = u'כניסה-לתחנה'


class ShinGimel(LockedRoom):
    def interact_with_player(self, player):
        if self.attributes['locked']:
            st = u'יושב כאן שג צעיר )קדצ 71 אם הייתי צריך לנחש(.\nהוא לא מכיר את אור לוי מרוב שהוא צעיר, והוא רוצה לראות חוגר.\n{}"בלי חוגר, אין יוצא ואין נכנס!"'
            if u'חוגר' not in player._get_inventory_names():
                st = st.format(u'כשאת מכניסה את היד לכיס להוציא את החוגר את קולטת שהוא לא שם.\nמאורעות אמש צפים ועולים ואת נזכרת שהחוגר בארונית שלך באופן-ספייס!\n')
            else:
                st = st.format(u'')
            return self.room_str(st)
        else:
            return self.room_str(u'הש.ג יושב משועמם ומשחק בכפתורים.\nייקח כמה דקות לצאת דרך הסבסבת כי כנראה שתתקעי באמצע')

    def set_name(self):
        self.name = u'ש.ג'

    def use(self, item, player):
        if item.name != u'חוגר':
            return u'הש.ג המנומנם משתעמם יותר.\n"עם כל הכבוד לפזמ, אני לא ממש יכול לזהות אותך עם {}"?'.format(item), False
        else:
            self.attributes['locked'] = False
            player.inventory.append(Item(u'קיטבג', u'שק חפצים גדול ומכוער'))
            return u'"וואי וואי" הש.ג מתלהב.\n"לפי החוגר הזה את משתחררת היום! מדהים! כמה פזמ אני נמס פה\nנראה לי שזה שלך." הוא מגיש לך את הקיטבג שלך!', False


class BackEntrance(LootRoom):
    def __init__(self, raw_name):
        super(BackEntrance, self).__init__(raw_name, [Item(u'תרנגולת', u'מצויינת לשניצלים')])

    def interact_with_player(self, player):
        s = u'בניגוד לבדרך כלל, הכניסה האחורית לתחנה פתוחה. בדיוק ממלאים את מכונת המשקאות.'
        if self.items:
            s += u'\nכל מיני ציפורים וחתולים מתרוצצים בחצר'
        return s

    def set_name(self):
        self.name = u'כניסה-אחורית'


class Coridor(MapTile):
    def interact_with_player(self, player):
        return u'המסדרון המקסים בין הכניסה האחורית והש.ג של גלצ'

    def set_name(self):
        self.name = u'מסדרון'


class Madregot1(MapTile):
    def interact_with_player(self, player):
        if not self.attributes['visited']:
            self.attributes['visit'] = True
            return \
                u'''מכאן אפשר לעלות ולרדת במדרגות של גלצ.
                נעשה הכל שהמדרגות במשחק ירגישו כמו החוויה האמיתית!'''
        else:
            return u'המדרגות הקסומות של גלצ. איך תתגעגעי ללעלות ולרדת פה!'

    def set_name(self):
        self.name = u'מדרגות'


class Madregot2(MapTile):
    def interact_with_player(self, player):
        return u'איזה כיף זה מדרגות!'

    def set_name(self):
        self.name = u'עוד-מדרגות'


class Madregot3(MapTile):
    def interact_with_player(self, player):
        return self.loc_string(u'כן... מדרגות... לאן בכלל התכוונת להגיע?', player.get_prev_room_name())

    def set_name(self):
        self.name = u'אפילו-עוד-מדרגות'


class Gag(PersonRoom):
    def __init__(self, raw_name):
        super(Gag, self).__init__(raw_name, u'מיקה', True, True)

    def set_name(self):
        self.name = u'גג'

    def interact_with_player(self, player):
        if self.attributes['locked']:
            return self.room_str(u'מה זה? דבורים? כנראה שאריות מהכוורות שהיו כאן.\nבכל מקרה, ככה אי אפשר לצאת לגג\nאולי כדאי לקרוא למדביר.')
        elif self.is_present:
            return u'מיקה יושבת על הגג ומעשנת.'
        else:
            return self.room_str(u'הגג הקסום של גלצ')

    def use(self, item, player):
        if (not self.attributes['locked']) or item.name != u'אייפון':
            return u'אין מה לעשות עם {} כאן'.format(item), False
        else:
            self.attributes['locked'] = False
            return u'''את מוציאה את האייפון ובמיומנות פותחת את הקבוצה הנכונה.
            "למישהו יש מספר של המדביר המזייף?"
            התשובות מגיעות מכל עבר ותוך שניות יש לך את הטלפון של המדביר האגדי.
            את מתקשרת אליו ולפני שהספקת לומר צ'יק צ'ק ג'וק הדבורים כבר לא שם.''', True

    def talk(self):
        self.is_present = False
        return u'''"מה? משתחררות? היום? סבבה. הולכת למזמ. חותמת שם.
        הכל אני צריכה לעשות פה!"
        מיקה מכבה את הסיגריה שלה ויורדת לכיוון מזמ.''' + u'TP_מזמ' , False


class Kitchen(PersonRoom):
    def __init__(self, raw_name):
        super(Kitchen, self).__init__(raw_name, u'טבח', True)

    def set_name(self):
        self.name = u'מטבח'

    def give(self, item, player):
        if item.name != u'תרנגולת':
            return u'הטבח לוקח את ה{0}.\nהוא מקמח, מוסיף ביצה, תבלינים... \nשניה לפני שהוא שם בתנור את אוזרת תושיה ומחזירה את ה{0} לידיים שלך.\nתחשבי מה את נותנת לטבחים פעם הבאה!'.format(item.name), False
        else:
            item.status = None
            player.inventory.append(Item(u'שניצל', u'זה יותר מזכיר סופגניה במילוי עוף, אבל זה יספק חיילת רעבה'))
            return\
                u'''מעולם לא ראית אושר כל כך גדול בעיניים של הטבח הזה. הוא לוקח את התרנגולת,
                תולש לה את הראש,
                מורט את הנוצות,
                שם בביצה,
                שם בקמח,
                מטגן - 
                וזורק עלייך שניצל. תתחדשי.''', False

    def use(self, item, player):
        return self.give(item, player)

    def interact_with_player(self, player):
        return u'נכנסת למטבח. השאלה היא למה. אפילו הטבח מופתע'

    def talk(self):
        return u'הוא לא רוצה לדבר איתך. הוא מבשל עכשיו.'


class Mzm(PersonRoom):
    def __init__(self, raw_name):
        super(Mzm, self).__init__(raw_name, u'מיקה', False)

    def set_name(self):
        self.name = u'מזמ'

    def interact_with_player(self, player):
        if not self.is_present:
            return u'מזכירות המערכת הנפלאה של מחלקת תכניות. אין כאן אף אחד, אפשר להדפיס מלא דפים!'
        elif player is not None and player.inventory[0].status is not None and u'מזמ' in player.inventory[0].status:
            player.inventory[0].sign_tati(self.name)
            return u'מיקה יושבת ומקלידה דברים. "נו? מה? לא חתמתי לך? בואי אני אחתום לך!"\nהחתימה הנכספת של מזמ על הטופס!'
        else:
            return u'מיקה יושבת ומקלידה דברים. "נו, איך הולך עם הטופס טיולים?"'

    def talk(self):
        return self.interact_with_player(None), False


class OpenSpace(LootRoom):
    def __init__(self, raw_name):
        super(OpenSpace, self).__init__(raw_name, [Item(u'אוזניות', u'אוזניות אולפן מהאיכות הטובה ביותר!'), Item(u'חוגר', u'חוגר עם התמונה הכי מדהימה בעולם'), Item(u'אייפון', u'אייפון 6 עם כיסוי מקסים של עצלנים')], locked=True)

    def interact_with_player(self, player):
        if self.attributes['locked']:
            return u'האופן ספייס הנפלא של גלצ.\nעם המחשבים המתקדמים ביותר שמספיקים לכל חיילי התחנה שרוצים לעבוד!\nועם הארונית שלך. הנעולה.'
        else:
            return self.room_str(u'האופן ספייס הנפלא של גלצ.\nעם המחשבים המתקדמים ביותר שמספיקים לכל חיילי התחנה שרוצים לעבוד!\nועם הארונית שלך')

    def set_name(self):
        self.name = u'אופן-ספייס'

    def use(self, item, player):
        if item.name != u'מפתח':
            return u'את משחקת קצת עם ה{}. ואז נמאס לך.'.format(item), False
        else:
            self.attributes['locked'] = False
            item.status = None
            return u'הארונית נפתחת בקליק. המפתח נשבר, אבל לא נורא. היא פתוחה עכשיו', True


class Afsenaut(PersonRoom):
    def __init__(self, raw_name):
        super(Afsenaut, self).__init__(raw_name, u'אפסנאי', False)
        self.attributes['paid'] = False
        self.attributes['zikui'] = False

    def give(self, item, player):
        if not self.attributes['zikui'] and item.name != u'קיטבג':
            return u'האפסנאי מתעצבן. "מה אני אעשה עם זה? ציוד מותק!"'.format(item.name), False
        elif item.name == u'קיטבג':
            item.status = None
            self.attributes['zikui'] = True
            return u'האפסנאי עובר על כל הציוד שלך. "מה זה מותק חסר פה זוג מדים. אני רואה שהזוג הזה לא שלך.\nבטח שאני אחתום על הטופס טיולים, רק תשלמי מתש 08 שקלים טבין ותקילין"', False
        if item.name != u'כסף':
            return u'האפסנאי בוחן את ה{}. אחרי כמה דקות של חשיבה הוא אומר "לא. זה לא שווה 08 שקל"'.format(
                item.name), False
        else:
            self.attributes['paid'] = True
            item.status = None
            player.inventory[0].sign_tati(u'אפסנאי')
            return u'האפסנאי מוציא את הארנק מהכיס שלו, שם את הכסף בפנים וקורץ לך.\n"יאללה תביאי טופס נחתום"\nהחתימה הקסומה ספוגת הנזלת נוספת לטופס!', False

    def interact_with_player(self, player):
        if player.inventory[0].status is not None:
            self.is_present = True
        if self.is_present:
            if not self.attributes['zikui']:
                return u'האפסנאי יושב פה ולא עושה כלום.'
            elif not self.attributes['paid']:
                return u'האפסנאי מחטט באף.\n"איבדת שילמת נשמה! מחכה לכסף!"'
            else:
                return u'האפסנאי אוכל דברים מהאף.'
        else:
            return u'אפסנאות גלצ. אין כאן אף אחד, כרגיל. נא לחזור בין 00:9 ל30:9'

    def set_name(self):
        self.name = u'אפסנאות'

    def talk(self):
        return u'האפסנאי עסוק מדי בשביל לדבר איתך.', False


class Shalishut(PersonRoom):
    def __init__(self, raw_name):
        super(Shalishut, self).__init__(raw_name, u'שלישה', True)
        self.attributes['needs'] = [u'מספריים', u'חוגר']
        self.attributes['hungry'] = True

    def set_name(self):
        self.name = u'שלישות'
    
    def give(self, item, player):
        if player.inventory[0].status:
            return u'השלישה כעוסה.\n"מה אני אעשה עם זה עכשיו? תבואי עם טופס טיולים מושלם ונדבר"', False
        elif (item.name != u'שניצל') and self.attributes['hungry']:
            return u'השלישה נראית מודאגת.\n"וואי מה זה לא בא לי בטוב עכשיו. אני מתה מרעב. נדבר אחר כך"', False
        else:
            if item.name == u'שניצל':
                self.attributes['hungry'] = False
                item.status = None
                return u'השלישה מתנפלת על השניצל ובולסת אותו ברעב גדול.\nמי ידע ששניצל ג-ד יכול להביא אושר כל כך גדול???', False
            elif item.name in self.attributes['needs']:
                self.attributes['needs'].remove(item.name)
                item.status = None
            else:
                return u'השלישה כעוסה. "מה אני אעשה עם {}? אני צריכה {}"'.format(item.name, u','.join(self.attributes['needs'])), False
            if self.attributes['needs'] == []:
                player.victory = True
                return u'היא לוקחת את החוגר והמספריים ובמיומנות פלאית גוזרת אותו לשני חלקים.', True
            else:
                return u'השלישה לוקחת את ה{} באושר'.format(item.name), False

    def interact_with_player(self, player):
        if (player is not None) and player.inventory[0].status is None:
            player.inventory[0].give_tati()
            return u'''השלישה מוציאה דברים בין השיניים. 
            "טוב אני מדפיסה לך טופס טיולים. זה כמה שניות. את רק צריכה חתימה מ-
            מזמ,
            אפסנאות,
            מהרסר
            ומטכנאי.
            תאמיני לי זה כיף"
            היא מביאה לך את טופס הטיולים הנכסף!'''
        elif (player is not None) and player.inventory[0].status == []:
            return u'השלישה מתרגשת. "טוב, שהו שה! תביאי לי רק חוגר ומשפריים ושיימנו פה!"'
        else:
            return u'השלישה מתאמנת על דקלום מהיר של "שרה שרה שיר שמח"'

    def talk(self):
        return self.interact_with_player(None)
